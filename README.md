Simple file sharing script using FTP 

```bash
usage: yshare.py [-h] [--keep-alive] [--port PORT] path

PyShare

positional arguments:
  path          File or directory to share

optional arguments:
  -h, --help    show this help message and exit
  --keep-alive  Do not close server after all files are done                                                          
  --port PORT   FTP server port

```
