#! /usr/bin/python3

import argparse
import logging
import socket
from pathlib import Path
from shutil import copyfile
from tempfile import TemporaryDirectory

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

logging.basicConfig(level=logging.INFO, format='%(message)s')
logging.getLogger('pyftpdlib').setLevel(logging.ERROR)


class ExtendedFTPHandler(FTPHandler):
    file_sent_count = 0
    total_file_count = 0

    def on_connect(self):
        self.server.close()

    def on_file_sent(self, file):
        self.file_sent_count += 1

        if self.total_file_count > 0:
            if self.file_sent_count >= self.total_file_count:
                logging.info('All files should now be downloaded...')
                self.server.close_all()


def get_total_file_count(path: Path) -> int:
    return sum(0 for element in path.rglob('*') if element.is_file())


def get_ip_address():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.connect(('1.1.1.1', 80))
        return s.getsockname()[0]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Yshare')
    parser.add_argument('path', type=str,
                        help='File or directory to share')
    parser.add_argument('--keep-alive', dest='keep_alive', action='store_true', default=False,
                        help='Do not close server after all files are done')
    parser.add_argument('--port', dest='port', default=8080,
                        help='FTP server port')
    args = parser.parse_args()

    to_share = Path(args.path)
    is_file = to_share.is_file()
    to_share = to_share.absolute()

    logging.info('Sharing {}, go to ftp://{}:{}'.format(to_share, get_ip_address(), args.port))

    if not to_share.exists():
        logging.error('"{}" does not exist'.format(to_share))
        exit(1)

    if is_file:
        temp_dir = TemporaryDirectory()
        temp_dir_path = Path(temp_dir.name)
        copyfile(to_share, temp_dir_path / to_share.name)
        to_share = temp_dir_path

    authorizer = DummyAuthorizer()
    authorizer.add_anonymous(str(to_share))

    handler = ExtendedFTPHandler
    handler.authorizer = authorizer
    if not args.keep_alive:
        handler.total_file_count = get_total_file_count(to_share)

    server = FTPServer(('0.0.0.0', args.port), handler)
    server.serve_forever()

    if is_file:
        temp_dir.cleanup()
